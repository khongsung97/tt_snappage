var dataMenu={data:[]};

$(document).ready(function() {
	$("#sortable-area1").sortable({
	    connectWith: ".group",
	    helper: function (e, li) {
	        copyHelper = li.clone().insertAfter(li);
	        return li.clone();
	    },
	    stop: function (e, ui) {
	        copyHelper && copyHelper.remove();
	    }

	});
	$("#sortable-area2").sortable({
		start: function(event, ui) {
			if(ui.item.context.nodeName != "LI") {
				window.itemSort = dataMenu.data.splice(ui.item.index(), 1);
			}
		},
		stop: function(event, ui) {
	        if(ui.item.context.nodeName != "LI") {
				dataMenu.data.splice(ui.item.index(), 0, window.itemSort[0]);
			}
	    },
	    receive: function (e, ui) {
	        copyHelper = null;
	        var file = $(ui.item).attr('value');
	        $.ajax({
	    		url: "json/" + file + ".json",
	    		dataType: 'json',
	    		success: (response) => {
	    			dataMenu.data.splice(ui.item.index(), 0, response);
	    			$(ui.item).replaceWith(draw(response));
    			}		    		
	    	})        
	    }
	});

	function draw(response) {
		var tag = document.createElement(response.tag);
		$(tag).addClass(response.class);
		if(typeof(response.content) != "string") {
			$.each(response.content, (k,v)=>{
				tag.appendChild(draw(v));
			})
		} else {
			$(tag).html(response.content);
			if(response.tag == "img") {
				$(tag).attr("src", response.src);
			}
		}
		return tag;
	}


	//form sửa
	$("body").on('click', '#sortable-area2 .item-food, .item-head', function(e) {
		idEdit         = $(this).index(); //positoin edit
		let data       = dataMenu.data[$(this).index()];
		let editorData = document.createElement("div");
		$(editorData).addClass('editor editorData');
		$(".left .editorData").length ? $(".left .editorData").remove() : "";
		$(".left").append(editorData);
		let titelForm = document.createElement("h2");
		let btnOK     = document.createElement("button");
		let btnCancel = document.createElement("button");
		$(titelForm).html("Editor item food");
		$(btnOK).addClass("editor__ok");
		$(btnOK).html("OK");
		$(btnCancel).addClass("editor__cancel");
		$(btnCancel).html("Cancel");
		$("body .editorData").append(titelForm);
		createInput(data);
		$("body .editorData").append(btnOK, btnCancel);
		if(e.target.className == "item__close") {
			dataMenu.data.splice($(this).index(), 1);
			$(this).remove();
		} else {
			$("body .editorData").css("display", "block");
			$("#sortable-area2").find(".active").removeClass("active");
			$(this).addClass("active");
		}
	})

	function createInput(data) {
		$.each(data.content, function(k,v) {
			if (v.tag != "small") {
				if(typeof(v.content) !== "string") {
					createInput(v);
				}
				else {
					let div   = document.createElement("div");
					let label = document.createElement("label");
					let input = document.createElement("input");
					$(label).html(v.label);
					$(input).attr("type", v.type);
					$(input).attr("name", v.name_input);
					$(input).attr("class", v.class);
					$(input).attr("value", v.content);
					$(div).append(label, input);
					$("body .editorData").append(div);
				}
			}
		});
	}

	$("body").on('change', '.file-image', function() {
		readImage(this);
	})

	function readImage(input) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$("#sortable-area2 .item").eq(idEdit).find(".file-image").attr('src', e.target.result);
		};
		reader.readAsDataURL(input.files[0]);
	}

	// replate when edited
	$("body").on('click', '.editor__ok', function() {
		editData(dataMenu.data[idEdit]);
		setTimeout(function(){
			$("#sortable-area2 .item").get(idEdit).replaceWith(draw(dataMenu.data[idEdit]));
		}, 200);
		$($("#sortable-area2 .item").get(idEdit)).addClass("active");
	})

	function editData(data) {
		$.each(data.content, function(k,v) {
			if (v.tag != "small") {
				if(typeof(v.content) !== "string") {
					editData(v);
				}else {
					if (v.tag == "img") {
						let src = $("input[name='"+v.name_input+"']").val();
						let arr = src.split("\\");
						src = arr[arr.length-1];
						pushImage();
						v.src = "./images/"+src;
					} else {
						v.content = $("input[name='"+v.name_input+"']").val();
					}
				}
			}
		});
	}

	function pushImage() {
		var myFormData = new FormData();
		myFormData.append("file", $('input[type="file"]').prop('files')[0]);
		$.ajax({
	        type: 'POST',               
	        processData: false, // important
	        contentType: false, // important
	        data: myFormData,
	        url: "images/upload.php",
	        success: function(data){
	            console.log(data);
	        }
	    });
	}

	// hidden edit form
	$("body").on('click', '.editor .editor__cancel', function() {
		$(this).parent().css("display", "none");
		$($("#sortable-area2 .item").get(idEdit)).removeClass("active");
	});

	// opent form save
	$(".modal-save, .modal-open").draggable();
	$(".modal-save__btn-cancel, .modal-open__btn-cancel").click(function() {
		$(this).parent().css('display', 'none');
	});
	$(".file__save").click(function(){
		$(".modal-save").toggle();
	});

	// open from file
	$(".file__open").click(function() {
		$(".modal-open").toggle();
		$.ajax({
			url: "./file/process.php",
			data: {listdata: "listdata"},
			type: "POST",
			success: function(response) {
				$('.modal-open select').empty();
				$.each(JSON.parse(response), function(k,v) {
					if(k>1) {
						let li = document.createElement("option");
						li.innerHTML = v;
						$(li).attr('value', v);
						$('.modal-open select').append(li);
					}
				})
			}
		});
	})

	// save to file
	$(".modal-save .modal-save__btn-save").click(function() {
		if(dataMenu.data != "") {
			data = JSON.stringify(dataMenu);
			fileName = $(this).parent().find('input').val();
			$.ajax({
				url: "./file/process.php",
				data: {save: data, fileName: fileName},
				type: "POST",
				success: function(response) {
					alert("Saved!");
				}
			});
		} else {
			alert("Nothing saved!")
		}
		$(this).parent().css('display', 'none');
	})

	//open file
	$(".modal-open .modal-open__btn-open").click(function() {
		let file = $(this).parent().find('select').val();
		$(this).parent().css('display', 'none');
		$.ajax({
			url: "./file/process.php",
			data: {file: file},
			type: "POST",
			success: function(response) {
				let obj = JSON.parse(response);
				$("#sortable-area2").empty();
				$.each(obj.data, function(k,v) {
					dataMenu.data.push(v);
					$("#sortable-area2").append(draw(v));
				})
			}
		});
	})
});